//EventListener: gameButtonLayer
document.querySelector('#stop-game')
    .addEventListener('click',function(){
        stopLayer.style.setProperty("display", "flex")
    });
document.querySelector('#select-game')
    .addEventListener('click',function(){
        selectLayer.style.setProperty("display", "flex")
    });
document.querySelector('#setting-game')
    .addEventListener('click',function(){
        settingLayer.style.setProperty("display", "flex")
    });
document.querySelector('#pause-game')
    .addEventListener('click',function(){
        pauseLayer.style.setProperty("display", "flex")
        gameStateSave = gameState
        gameState = 0
        setFrameRate(0)
    });
document.querySelector('#reset-game')
    .addEventListener('click',function(){
        resetLayer.style.setProperty("display", "flex")
    });
document.querySelector('#resize-game')
    .addEventListener('click',function(){
        resizeLayer.style.setProperty("display", "flex")
    });

//EventListener: mainLayer
document.querySelector("#main-start-game")
    .addEventListener("click",(event)=>{
        setDefault()
        selectLayer.style.setProperty("display", "flex")
    })

//EventListener: selectLayer
document.querySelector("#select-1")
    .addEventListener("click",(event)=>{
        if(gameState != 1){
            if(gameStateSave != 0){
                document.getElementById(`select-${gameState}`).classList.remove("active")
            }            
            document.getElementById("select-1").classList.add("active")
            gameState = 1
            gameStateSave = 1
            init()
        }
        gameButtonLayer.style.setProperty("display", "flex")
        selectLayer.style.setProperty("display", "none")        
    })
document.querySelector("#select-2")
    .addEventListener("click",(event)=>{
        if(gameState != 2){
            if(gameStateSave != 0){
                document.getElementById(`select-${gameState}`).classList.remove("active")
            }            
            document.getElementById("select-2").classList.add("active")
            gameState = 2
            gameStateSave = 2
            init()
        }
        gameButtonLayer.style.setProperty("display", "flex")
        selectLayer.style.setProperty("display", "none")        
    })
document.querySelector("#select-3")
    .addEventListener("click",(event)=>{
        if(gameState != 3){
            if(gameStateSave != 0){
                document.getElementById(`select-${gameState}`).classList.remove("active")
            }            
            document.getElementById("select-3").classList.add("active")
            gameState = 3
            gameStateSave = 3
            init()
        }
        gameButtonLayer.style.setProperty("display", "flex")
        selectLayer.style.setProperty("display", "none")        
    })
document.querySelector("#select-return")
    .addEventListener("click",(event)=>{
        selectLayer.style.setProperty("display", "none")
        if(gameStateSave == 0){
            gameButtonLayer.style.setProperty("display", "none")
        }        
    })

//EventListener: settingLayer
document.querySelector("#setting-framerate")
    .addEventListener("click",(event)=>{
        framerateLayer.style.setProperty("display", "flex")
    })
document.querySelector("#setting-style")
    .addEventListener("click",(event)=>{
        styleLayer.style.setProperty("display", "flex")
    })
document.querySelector("#setting-survival")
    .addEventListener("click",(event)=>{
            survivalLayer.style.setProperty("display", "flex")
    })
document.querySelector("#setting-reproduction")
    .addEventListener("click",(event)=>{
        reproductionLayer.style.setProperty("display", "flex")
    })
document.querySelector("#setting-return")
    .addEventListener("click",(event)=>{
        settingLayer.style.setProperty("display", "none")
    })

//EventListener: settingLayer-framerateLayer
document.querySelector("#framerate-5")
    .addEventListener("click",(event)=>{
        if(framerate != 5){
            document.getElementById("framerate-5").classList.add("active")
            document.getElementById(`framerate-${framerate}`).classList.remove("active")
            framerate = 5
            setFrameRate(framerate)
        }
    })
document.querySelector("#framerate-15")
    .addEventListener("click",(event)=>{
        if(framerate != 15){
            document.getElementById("framerate-15").classList.add("active")
            document.getElementById(`framerate-${framerate}`).classList.remove("active")
            framerate = 15
            setFrameRate(framerate)
        }
    })
document.querySelector("#framerate-30")
    .addEventListener("click",(event)=>{
        if(framerate != 30){
            document.getElementById("framerate-30").classList.add("active")
            document.getElementById(`framerate-${framerate}`).classList.remove("active")
            framerate = 30
            setFrameRate(framerate)
        }
    })
document.querySelector("#framerate-return")
    .addEventListener("click",(event)=>{
        framerateLayer.style.setProperty("display", "none")
    })

//EventListener: settingLayer-styleLayer
document.querySelector("#style-0")
    .addEventListener("click",(event)=>{
        if(style != 0){
            document.getElementById("style-0").classList.add("active")
            document.getElementById(`style-${style}`).classList.remove("active")
            style = 0
        }
    })
document.querySelector("#style-1")
    .addEventListener("click",(event)=>{
        if(style != 1){
            document.getElementById("style-1").classList.add("active")
            document.getElementById(`style-${style}`).classList.remove("active")
            style = 1
        }
    })
document.querySelector("#style-2")
    .addEventListener("click",(event)=>{
        if(style != 2){
            document.getElementById("style-2").classList.add("active")
            document.getElementById(`style-${style}`).classList.remove("active")
            style = 2
        }
    })
document.querySelector("#style-return")
    .addEventListener("click",(event)=>{
        styleLayer.style.setProperty("display", "none")
    })

//EventListener: settingLayer-survivalLayer
document.querySelector("#survival-loneliness")
    .addEventListener("click",(event)=>{
        lonelinessLayer.style.setProperty("display", "flex")
    })
document.querySelector("#survival-overpopulation")
    .addEventListener("click",(event)=>{
        overpopulationLayer.style.setProperty("display", "flex")
    })
document.querySelector("#survival-return")
    .addEventListener("click",(event)=>{
        survivalLayer.style.setProperty("display", "none")
    })

//EventListener: settingLayer-survivalLayer-lonelinessLayer
document.querySelector("#loneliness-1")
    .addEventListener("click",(event)=>{
        if(loneliness != 1){
            document.getElementById("loneliness-1").classList.add("active")
            document.getElementById(`loneliness-${loneliness}`).classList.remove("active")
            loneliness = 1
        }
    })
document.querySelector("#loneliness-2")
    .addEventListener("click",(event)=>{
        if(loneliness != 2){
            document.getElementById("loneliness-2").classList.add("active")
            document.getElementById(`loneliness-${loneliness}`).classList.remove("active")
            loneliness = 2
        }
    })
document.querySelector("#loneliness-3")
    .addEventListener("click",(event)=>{
        if(loneliness != 3){
            document.getElementById("loneliness-3").classList.add("active")
            document.getElementById(`loneliness-${loneliness}`).classList.remove("active")
            loneliness = 3
        }
    })
document.querySelector("#loneliness-return")
    .addEventListener("click",(event)=>{
        lonelinessLayer.style.setProperty("display", "none")
    })

//EventListener: settingLayer-survivalLayer-overpopulationLayer
document.querySelector("#overpopulation-2")
    .addEventListener("click",(event)=>{
        if(overpopulation != 2){
            document.getElementById("overpopulation-2").classList.add("active")
            document.getElementById(`overpopulation-${overpopulation}`).classList.remove("active")
            overpopulation =2
        }
    })
document.querySelector("#overpopulation-3")
    .addEventListener("click",(event)=>{
        if(overpopulation != 3){
            document.getElementById("overpopulation-3").classList.add("active")
            document.getElementById(`overpopulation-${overpopulation}`).classList.remove("active")
            overpopulation = 3
        }
    })
document.querySelector("#overpopulation-4")
    .addEventListener("click",(event)=>{
        if(overpopulation != 4){
            document.getElementById("overpopulation-4").classList.add("active")
            document.getElementById(`overpopulation-${overpopulation}`).classList.remove("active")
            overpopulation = 4
        }
    })
document.querySelector("#overpopulation-return")
    .addEventListener("click",(event)=>{
        overpopulationLayer.style.setProperty("display", "none")
    })

//EventListener: settingLayer-reproductionLayer
document.querySelector("#reproduction-2")
    .addEventListener("click",(event)=>{
        if(reproduction != 2){
            document.getElementById("reproduction-2").classList.add("active")
            document.getElementById(`reproduction-${reproduction}`).classList.remove("active")
            reproduction =2
        }
    })
document.querySelector("#reproduction-3")
    .addEventListener("click",(event)=>{
        if(reproduction != 3){
            document.getElementById("reproduction-3").classList.add("active")
            document.getElementById(`reproduction-${reproduction}`).classList.remove("active")
            reproduction = 3
        }
    })
document.querySelector("#reproduction-4")
    .addEventListener("click",(event)=>{
        if(reproduction != 4){
            document.getElementById("reproduction-4").classList.add("active")
            document.getElementById(`reproduction-${reproduction}`).classList.remove("active")
            reproduction = 4
        }
    })
document.querySelector("#reproduction-return")
    .addEventListener("click",(event)=>{
        reproductionLayer.style.setProperty("display", "none")
    })

//EventListener: stopLayer
document.querySelector("#stop-yes")
    .addEventListener("click",(event)=>{
        stopLayer.style.setProperty("display", "none")
        document.getElementById(`select-${gameState}`).classList.remove("active")
        gameState = 0
        gameStateSave = 0
        setDefault()
        init()
        gameButtonLayer.style.setProperty("display", "none")
    })
document.querySelector("#stop-no")
    .addEventListener("click",(event)=>{
        stopLayer.style.setProperty("display", "none")
    })

//EventListener: pauseLayer
document.querySelector("#pause-resume")
    .addEventListener("click",(event)=>{
        pauseLayer.style.setProperty("display", "none")
        gameState = gameStateSave
        setFrameRate(framerate)
    })

//EventListener: resetLayer
document.querySelector("#reset-yes")
    .addEventListener("click",(event)=>{
        resetLayer.style.setProperty("display", "none")
        document.getElementById(`framerate-${framerate}`).classList.remove("active")
        document.getElementById(`style-${style}`).classList.remove("active")
        document.getElementById(`loneliness-${loneliness}`).classList.remove("active")
        document.getElementById(`overpopulation-${overpopulation}`).classList.remove("active")
        document.getElementById(`reproduction-${reproduction}`).classList.remove("active")
        setDefault()
        init()
    })
document.querySelector("#reset-no")
    .addEventListener("click",(event)=>{
        resetLayer.style.setProperty("display", "none")
    })

//EventListener: resizeLayer
document.querySelector("#resize-yes")
    .addEventListener("click",(event)=>{
        resize()        
        resizeLayer.style.setProperty("display", "none")
    })
document.querySelector("#resize-no")
    .addEventListener("click",(event)=>{
        resizeLayer.style.setProperty("display", "none")
    })