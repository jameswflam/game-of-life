let backgroundLayer = document.querySelector("#background")
let mainLayer = document.querySelector("#main")
let creditLayer = document.querySelector("#credit")
let canvasLayer = document.querySelector(".canvas-row")

const unitLength  =  20; //The width and height of a box
const boxColor  =  150; //The color of the box
const strokeColor  =  50; //The color of the stroke of the box
let columns; //Number of columns in our game of life. It is determined by the width of the container and unitLength
let rows; //Number of rows in our game of life. It is determined by the height of the container and unitLength
let currentBoard; //The states of the board of the current generation
let nextBoard; //The states of the board of the next generation. It is determined by the current generation

const boxColorArray = [[255, 51, 51], [255, 153, 51], [255, 255, 51], [51, 255, 51], [51, 255, 255], [51, 153, 255], 
    [51, 51, 255], [153, 51, 255], [255, 51, 255], [255, 51, 153]] //10 color

let framerate = 15 //default framerate
let loneliness = 2 //default loneliness rule
let overpopulation = 3 //default overpopulation rule
let reproduction = 3 //default reproduction rule

//p5 function:called once when the program starts, setting up the initial values
function setup(){
    /* Set the canvas to be under the element #canvas*/
    const canvas = createCanvas(windowWidth*0.98-windowWidth*0.98%unitLength,windowHeight*0.9-windowHeight*0.9%unitLength);
    canvas.parent(document.querySelector('#canvas'));

    /*Calculate the number of columns and rows */
    columns = floor(width/ unitLength);
    rows = floor(height / unitLength);

    /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
    currentBoard = [];
    nextBoard = [];

    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }
    // Now both currentBoard and nextBoard are array of array of undefined values.
    init();  // Set the initial values of the currentBoard and nextBoard
}
 
//Initialize/reset the board state
function  init() {
    setFrameRate(framerate)
    for (let  i  =  0; i  <  columns; i++) {
        for (let  j  =  0; j  <  rows; j++) {
            currentBoard[i][j] = random() > 0.8? 1: 0;
            nextBoard[i][j] = 0;
        }
    }
    mainLayer.style.setProperty("display", "flex")
}

//p5 function: called directly after setup()
//run many times. It is going to run once per frame. 
//If you have a frame rate of 30 frames per second, then draw() runs 30 times per second!
function draw(){
    background(255);
    generate(loneliness, overpopulation, reproduction);
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1){ //has live
                let index
                if(i > j){
                    index = (i-j)%10
                }
                else{
                    index = (j-i)%10
                }
                fill(boxColorArray[index][0],boxColorArray[index][1],boxColorArray[index][2]);               
            }else{ //no live
                fill(255);
            } 
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
}

function generate(lonelinessIndex, overpopulationIndex, reproductionIndex) {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if( i=== 0 && j ===0 ){
                        // the cell itself is not its own neighbor
                        continue;
                    }
                    // The modulo operator is crucial for wrapping on the edge
                    neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                }
            }

            // Rules of Life
            if (currentBoard[x][y] == 1 && neighbors < lonelinessIndex) {
                // Die of Loneliness
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 1 && neighbors > overpopulationIndex) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == reproductionIndex) {
                // New life due to Reproduction
                nextBoard[x][y] = 1;
            } else {
                // Stasis
                nextBoard[x][y] = currentBoard[x][y];
            }
        }
    }

    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}

/**
 * When mouse is dragged
 */
function mouseDragged() {
    //If the mouse coordinate is outside the board
    if(mouseX > unitLength * columns || mouseY > unitLength * rows){
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    fill(boxColor);
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

/**
 * When mouse is pressed
 */
function mousePressed() {
    noLoop();
    mouseDragged();    
}

/**
 * When mouse is released
 */
function mouseReleased() {
    loop();
}

document.querySelector("#credit-back-main")
    .addEventListener("click",(event)=>{
        creditLayer.style.setProperty("display", "none")
    })
    
document.querySelector("#main-credit")
    .addEventListener("click",(event)=>{
        creditLayer.style.setProperty("display", "flex")
    })