﻿let mainLayer = document.querySelector("#main")
let gameButtonLayer = document.querySelector("#game-button-list")
let selectLayer = document.querySelector("#select")
let stopLayer = document.querySelector("#stop")
let settingLayer = document.querySelector("#setting")
let framerateLayer = document.querySelector("#framerate")
let styleLayer = document.querySelector("#style")
let survivalLayer = document.querySelector("#survival")
let lonelinessLayer = document.querySelector("#loneliness")
let overpopulationLayer = document.querySelector("#overpopulation")
let reproductionLayer = document.querySelector("#reproduction")
let pauseLayer = document.querySelector("#pause")
let resetLayer = document.querySelector("#reset")
let resizeLayer = document.querySelector("#resize")

let unitLength  =  20; //The width and height of a box
const boxColor  =  150; //The color of the box
const strokeColor  =  50; //The color of the stroke of the box
let columns; //Number of columns in our game of life. It is determined by the width of the container and unitLength
let rows; //Number of rows in our game of life. It is determined by the height of the container and unitLength
let currentBoard; //The states of the board of the current generation
let nextBoard; //The states of the board of the next generation. It is determined by the current generation


const boxColorArray = [
    [[255, 51, 51], [255, 153, 51], [255, 255, 51], [51, 255, 51], [51, 255, 255], [51, 153, 255], 
    [51, 51, 255], [153, 51, 255], [255, 51, 255], [255, 51, 153]], //rainbow style
    [[254, 238, 2], [251, 199, 19], [247, 159, 36], [251, 98, 18], [255, 38, 0], [251, 40, 144]], //warm style
    [[127, 209, 0], [0, 181, 0], [2, 193, 175], [1, 111, 196], [59, 72, 186], [116, 33, 175]] //cold style
    ] 
let gameState = 0 //1: random 2: gosper glider gun 3:p144 0:default(game pause/before select)
let gameStateSave = 0; //save game state for game pause and mode select

let framerate //default framerate
let loneliness //default loneliness rule
let overpopulation //default overpopulation rule
let reproduction //default reproduction rule
let style //default color syle 0:rainbow, 1:warm, 2:cold
let boardLifeCount //-1:no life, 0:new life, >0:count life period

const pattern2= `
...........
.....xx....
.....xx....
...........
...........
...........
...........
...........
...........
...........
...........
.....xxx...
....x...x..
...x.....x.
...x.....x.
......x....
....x...x..
.....xxx...
......x....
...........
...........
...xxx.....
...xxx.....
..x...x....
...........
.xx...xx...
...........
...........
...........
...........
...........
...........
...........
...........
...........
...xx......
...xx......
`
const pattern3 =`
..............................
.xx........................xx.
.xx........................xx.
...................xx.........
..................x..x........
...................xx.........
...............x..............
..............x.x.............
.............x...x............
.............x..x.............
..............................
.............x..x.............
............x...x.............
.............x.x..............
..............x...............
.........xx...................
........x..x..................
.........xx...................
.xx........................xx.
.xx........................xx.
..............................
`

//p5 function:called once when the program starts, setting up the initial values
function setup(){
    /* Set the canvas to be under the element #canvas*/
    const canvas = createCanvas(windowWidth*0.98-windowWidth*0.98%unitLength,windowHeight*0.6-windowHeight*0.6%unitLength);
    canvas.parent(document.querySelector('#canvas'));

    /*Calculate the number of columns and rows */
    if(windowWidth < 400||windowHeight <800){
        unitLength = 10
    }
    columns = floor(width/ unitLength);
    rows = floor(height / unitLength);
    /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
    currentBoard = [];
    nextBoard = [];
    boardLifeCount = []

    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
        boardLifeCount[i] = []
    }
    // Now both currentBoard and nextBoard are array of array of undefined values.
    init();  // Set the initial values of the currentBoard and nextBoard
    mainLayer.style.setProperty("display", "flex")
}
 
//Initialize/reset the board state
function init() {
    setFrameRate(framerate)
    for (let  i  =  0; i  <  columns; i++) {
        for (let  j  =  0; j  <  rows; j++) {
            if(gameState == 1){ //random mode
                if(random() > 0.8){
                    currentBoard[i][j] = 1
                    boardLifeCount[i][j] = 0
                }
                else{
                    currentBoard[i][j] = 0
                    boardLifeCount[i][j] = -1
                }                
                nextBoard[i][j] = 0;
            }
            else{
                currentBoard[i][j] =  0;;
                boardLifeCount[i][j] = -1
                nextBoard[i][j] =  0;
            }
        }
    }
    if(gameState == 2){//gosper glider gun mode
        let patternArray = getPatternArray(pattern2)
        for(let array of patternArray){
            currentBoard[array[0]][array[1]] = 1
            boardLifeCount[array[0]][array[1]] = 0
        }
    }
    else if(gameState == 3){
        let patternArray = getPatternArray(pattern3)
        let positionColumn = floor(columns/2)-15
        let positionRow = floor(rows/2)-10
        for(let array of patternArray){            
            currentBoard[positionColumn + array[0]][positionRow + array[1]] = 1
            boardLifeCount[positionColumn + array[0]][positionRow + array[1]] = 0
        }
    }
}

//p5 function: called directly after setup()
//run many times. It is going to run once per frame. 
//If you have a frame rate of 30 frames per second, then draw() runs 30 times per second!
function draw(){
    background(255);
    generate(loneliness, overpopulation, reproduction);
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1){ //has live
                let index
                if(i > j){
                    if(style == 0){
                        index = (i-j)%10
                    }
                    else{
                        index = (i-j)%6
                    }
                }
                else{
                    if(style == 0){
                        index = (j-i)%10
                    }
                    else{
                        index = (j-i)%6
                    }
                }
                if(boardLifeCount[i][j] == 0){
                    fill(boxColorArray[style][index][0],boxColorArray[style][index][1],boxColorArray[style][index][2])
                }
                else{
                    let colorArray = [0, 0, 0]
                    for(let key in boxColorArray[style][index]){
                        colorArray[key] = Math.max(boxColorArray[style][index][key] - boardLifeCount[i][j]*2, 0)
                    }
                    fill(colorArray[0], colorArray[1], colorArray[2])
                }                             
            }
            else{ //no live
                fill(255);
            } 
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
}

function generate(lonelinessIndex, overpopulationIndex, reproductionIndex) {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if( i=== 0 && j ===0 ){
                        // the cell itself is not its own neighbor
                        continue;
                    }
                    // The modulo operator is crucial for wrapping on the edge
                    if(gameState == 2){
                        if(x != columns-1){
                            neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                        }                        
                    }
                    else{
                        neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                    }                 
                }
            }
            // Rules of Life
            if (currentBoard[x][y] == 1 && neighbors < lonelinessIndex) {
                // Die of Loneliness
                nextBoard[x][y] = 0;
                boardLifeCount[x][y] = -1
            } else if (currentBoard[x][y] == 1 && neighbors > overpopulationIndex) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;
                boardLifeCount[x][y] = -1
            } else if (currentBoard[x][y] == 0 && neighbors == reproductionIndex) {
                // New life due to Reproduction
                nextBoard[x][y] = 1;
                boardLifeCount[x][y] = 0            
            } else {// Stasis
                nextBoard[x][y] = currentBoard[x][y];
                if(currentBoard[x][y] == 1){
                    boardLifeCount[x][y] += 1
                }
            }
        }
    }
    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}


/**
 * When mouse is dragged
 */
 function mouseDragged() {
    if(gameState != 0){
        //If the mouse coordinate is outside the board
        if(mouseX > unitLength * columns || mouseY > unitLength * rows){
            return;
        }
        const x = Math.floor(mouseX / unitLength);
        const y = Math.floor(mouseY / unitLength);
        currentBoard[x][y] = 1;
        fill(boxColor);
        stroke(strokeColor);
        rect(x * unitLength, y * unitLength, unitLength, unitLength);
    }    
}

/**
 * When mouse is pressed
 */
function mousePressed() {
    if(gameState != 0){
        noLoop();
        mouseDragged(); 
    }    
}

/**
 * When mouse is released
 */
function mouseReleased() {
    if(gameState != 0){
        loop();
    }
}

function setDefault(){
    framerate = 30 //default framerate
    setFrameRate(30)
    document.getElementById(`framerate-${framerate}`).classList.add("active")
    
    style = 0 //default style:rainbow
    document.getElementById(`style-${style}`).classList.add("active")  
    
    loneliness = 2 //default loneliness rule
    document.getElementById(`loneliness-${loneliness}`).classList.add("active")
    
    overpopulation = 3 //default overpopulation rule
    document.getElementById(`overpopulation-${overpopulation}`).classList.add("active")
    
    reproduction = 3 //default reproduction rule
    document.getElementById(`reproduction-${reproduction}`).classList.add("active")
}

function resize(){
    //resizeCanvas(windowWidth*0.98-windowWidth*0.98%unitLength,windowHeight*0.6-windowHeight*0.6%unitLength);
    resizeCanvas(windowWidth-110,windowHeight);
    setup()
}

function getPatternArray(pattern){
    let lines = pattern.split('\n')
    let patternArray = []
    lines.pop()
    lines.shift()
    lines = lines.map((line, dx) =>
        line.split('').map((char, dy) => {
            let value = char === '.' ? 0 : 1
            return {
            value,
            dx,
            dy,
            }
        }),
    )
    for(let key in lines){
        for(let object of lines[key]){
            if(object.value == 1){
            patternArray.push([object.dy, object.dx])
            }      
        }
    }  
    return patternArray //[[v,x,y],[v,x,y],.....]
}